/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Negocio;

import Excepciones.ClienteException;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ControladorConexion{
    //sockets
    private DatagramSocket clientSocketRecive;  
    private DatagramSocket clientSocketSend; 
    
    private MulticastSocket clientMulticastSocket;
    //multi
    private InetAddress IP_Multicast = null;
    private final String dirMulticast = "225.5.4.42";
    private int PORT_Multicast = 54320;
    //server
    private InetAddress IP_Host;
    private int PORT_Host;
    //datos user
    private String USUARIO;        
    private boolean estaLogueado = false;
    private final int PORT_USUARIO = 54321;
   
    //cuando envío
    int nroSeqEstadoCliente;
    int ultimoNroSeqEnvEstadoCliente;
    
    //cuando recibo
    int nroSeqEstadoServidor;
    int ultimoNroSeqRcvEstadoServidor;
    
    //cuando envio multicast
    int nroSeqEstadoClienteMulticast;
    int ultimoNroSeqEnvEstadoClienteMulticast;
    
    //cuando recibo multicast
    int nroSeqEstadoSrvMulticast;
    int ultimoNroSeqRcvEstadoSrvMulticast;
    
    private static final String LOGIN = "LOGIN";
    private static final String LOGOUT = "LOGOUT";
    private static final String GET_CONNECTED = "GET_CONNECTED";
    private static final String MESSAGE = "MESSAGE";
    private static final String PRIVATE_MESSAGE = "PRIVATE_MESSAGE";    
    private static final String RELAYED_MESSAGE = "RELAYED_MESSAGE";
    private static final String CONNECTED = "CONNECTED";
    private static final String GOODBYE = "GOODBYE"; 
    
    private final Semaphore mutexEnvio = new Semaphore(1);
    private final Semaphore mutexRecive = new Semaphore(1);
    
    
    //lista de mensajes a enviar
    private List<String> mensajesAEnviar;   
    private List<String> mensajesAEnviarMulticast;
    private List<String> mensajesRecibidos;
    
     
    private boolean estaEnviandoUnicast;
    private boolean estaEnviandoMulticast;
    
    //Usuarios
    private List<String> listaDeUsuarios;
            
    //tiempo de espera
    private int tiempoDeEspera;
            
    private static ControladorConexion instance = null;
    
    public static ControladorConexion getinstance(){
        if (instance == null){
            instance = new ControladorConexion();
        }
        return instance;
    }

    public ControladorConexion() {
        try {
            IP_Multicast = InetAddress.getByName(dirMulticast);
            Init();
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(ControladorConexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void Init(){
        listaDeUsuarios = new ArrayList<>();

        //mensajesAEnviar = new ArrayList<>();
        mensajesAEnviar = Collections.synchronizedList(new ArrayList<String>());
        mensajesAEnviarMulticast = Collections.synchronizedList(new ArrayList<String>());
        mensajesRecibidos = Collections.synchronizedList(new ArrayList<String>());
        

        nroSeqEstadoCliente = 0;
        ultimoNroSeqEnvEstadoCliente = 1;
        nroSeqEstadoServidor = 0;
        ultimoNroSeqRcvEstadoServidor = 1;
        nroSeqEstadoClienteMulticast = 0;
        ultimoNroSeqEnvEstadoClienteMulticast = 1;
        nroSeqEstadoSrvMulticast=0;
        ultimoNroSeqRcvEstadoSrvMulticast = -1;

        estaEnviandoUnicast=false;
        estaEnviandoMulticast=false;

        tiempoDeEspera = 60; // 60 * 500milisegundos = 30segundos
        CrearSocket();
        CrearSocketMulticast();
        
    }
    private void ValidarDatos(String host, String puerto, String usuario) throws ClienteException{
        if ("".equals(host.trim()) || "".equals(puerto.trim()) || "".equals(usuario.trim())){
            throw new ClienteException("Hay que completar los 3 campos!!!");
        }
        try{
            PORT_Host = Integer.parseInt(puerto);
        }catch (NumberFormatException e){
            throw new ClienteException("El puerto no es numérico!!!");
        }
        USUARIO = usuario;
        try {
            IP_Host = InetAddress.getByName(host);
        } catch (UnknownHostException ex) {
            throw new ClienteException("Error al obtener dirección del Host!!!");
        }
        
    }
    
    private void CrearSocket(){
        try {
            clientSocketRecive = new DatagramSocket(PORT_USUARIO); 
            clientSocketSend = new DatagramSocket();
        } catch (SocketException ex) {
            //Logger.getLogger(ControladorConexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void CrearSocketMulticast(){
        try {
            clientMulticastSocket = new MulticastSocket(PORT_Multicast);
            clientMulticastSocket.joinGroup(IP_Multicast); 
            
        } catch (IOException ex) {
            //Logger.getLogger(ControladorConexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private boolean EnviarPaquete(String mensaje) throws IOException{
        byte[] sendData = new byte[512];
        sendData = mensaje.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IP_Host, PORT_Host); 
        boolean salida = false;
        //try {                            
        clientSocketSend.send(sendPacket);                 
        salida = true;
        //} catch (IOException ex) {
        //    throw new ClienteException("Error al enviar paquete UDP!!!");
        //} 
        return salida;
    }
    public String RecibirPaquete() throws IOException{
        String msg;
        //try {
        byte[] receiveData = new byte[512]; 
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        clientSocketRecive.receive(receivePacket);
        msg = new String(receiveData, 0, receiveData.length);
        //System.out.println("Hilo Private : "+msg); 
        mensajesRecibidos.add(msg);
            //System.out.println(msg); 
        //} catch (IOException ex) {
        //    throw new ClienteException("Error al enviar paquete UDP!!!");
        //}
        return msg;
    }
    public String RecibirPaqueteMulticast() throws IOException{
        String msg;
        //try {
        byte[] receiveData = new byte[512]; 
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);            
        clientMulticastSocket.receive(receivePacket);
        msg = new String(receiveData, 0, receiveData.length);
        //System.out.println("Hilo Multi : "+msg); 
        //} catch (IOException ex) {
        //    throw new ClienteException("Error al enviar paquete UDP!!!");
        //} 
        return msg;
    }
    
    public void CerrarSockets(){
        clientSocketRecive.close();
        clientMulticastSocket.close();
        clientSocketSend.close();
    }
    private void EnvioMSGyEsperoACK(String mensaje) throws ClienteException, IOException{
        long tiempoLocal = 0;
        try {
            estaEnviandoUnicast = true;
            while (ultimoNroSeqEnvEstadoCliente != nroSeqEstadoCliente && tiempoLocal < tiempoDeEspera){ //no me llegó ack correspondiente
                mutexEnvio.acquire();
                EnviarPaquete(mensaje);
                mutexEnvio.release();
                //LUEGO tengo que obtener respuesta positiva, osea el ACK, sino reenviar hasta recibir el ack                     
                Thread.sleep(500);  
                tiempoLocal++;
                
            }    
            estaEnviandoUnicast = false;
            if (tiempoLocal >= tiempoDeEspera){
                throw new ClienteException("TIMEOUT Tiempo de espera agotado");
            }else{//llegó el ack
                mutexRecive.acquire();
                nroSeqEstadoCliente = ultimoNroSeqEnvEstadoCliente == 0 ? 1 : 0; 
                mutexRecive.release();            
            }
            
        } catch (InterruptedException ex) {
            //Logger.getLogger(ControladorConexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void EnvioMSGMultiyEsperoACKM(String mensaje) throws ClienteException, IOException{
        long tiempoLocal = 0;
        try {
            estaEnviandoMulticast = true;
            while (ultimoNroSeqEnvEstadoClienteMulticast != nroSeqEstadoClienteMulticast && tiempoLocal < tiempoDeEspera){ //no me llegó ack correspondiente
                mutexEnvio.acquire();
                EnviarPaquete(mensaje);
                mutexEnvio.release();
                //LUEGO tengo que obtener respuesta positiva, osea el ACKM, sino reenviar hasta recibir el ack                     
                Thread.sleep(500);  
                tiempoLocal++;
            }    
            estaEnviandoMulticast = false;
            if (tiempoLocal >= tiempoDeEspera){
                throw new ClienteException("TIMEOUT Tiempo de espera agotado");
            }else{              
                mutexRecive.acquire();
                nroSeqEstadoClienteMulticast = ultimoNroSeqEnvEstadoClienteMulticast == 0 ? 1 : 0;  
                mutexRecive.release();
            }
           
        } catch (InterruptedException ex) {
            //Logger.getLogger(ControladorConexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /////Casos de uso públicos
    //SESSION
    public void Login(String host, String puerto, String usuario) throws ClienteException, IOException{                        
        ValidarDatos(host, puerto, usuario);        
        nroSeqEstadoCliente = 0;
        String mensaje = LOGIN + " " + getUSUARIO() + " " + nroSeqEstadoCliente;
        EnvioMSGyEsperoACK(mensaje);  
        setEstaLogueado(true); 
    }
    
    public void Logout() throws ClienteException, IOException{
        String mensaje = LOGOUT + " " + nroSeqEstadoCliente;           
        EnvioMSGyEsperoACK(mensaje);
        setEstaLogueado(false); 
        //CerrarSockets();                
    }
    //SEND
    public void EnviarMensajePrivado(String mensaje) throws ClienteException, IOException{
        mensaje = mensaje + " " + nroSeqEstadoCliente;
        
        EnvioMSGyEsperoACK(mensaje);
        if (!mensajesAEnviar.isEmpty())
            mensajesAEnviar.remove(0);
                            
    }    
    public void EnviarMensajeMulticast(String mensaje) throws ClienteException, IOException{
        mensaje = mensaje + " " +nroSeqEstadoClienteMulticast;
        //System.out.println(mensaje + " Numero de seq : " + nroSeqEstadoClienteMulticast);
        //EnviarPaquete(mensaje);        
        //Ahora tengo que recibir el ACK, sino reintentar  
        EnvioMSGMultiyEsperoACKM(mensaje);
        if (!mensajesAEnviarMulticast.isEmpty()){
            mensajesAEnviarMulticast.remove(0);
        }
        
    }        
    
    
    private void EnviarACK(int nro_seq, String tipo) throws ClienteException, IOException{
        
        //String seq = msgReciv.substring(msgReciv.trim().length()-1,msgReciv.trim().length());
        //int nro_seq = Integer.parseInt(seq.trim()); //nrp seq recibido

        //si me llega un numero de secuencia igual al ultimo ack recibido entonces
        //es porq mi ack no llego, lo envio de nuevo, y omito el mensaje, porq es duplicado
        if (tipo.equalsIgnoreCase("UNICAST")){
            if (nro_seq == ultimoNroSeqRcvEstadoServidor){
                EnviarPaquete("ACK "+nro_seq);
            }else{
                int nro = nro_seq == 0 ? 0 : 1 ;
                EnviarPaquete("ACK "+nro);
                ultimoNroSeqRcvEstadoServidor = nro;             
            }                    
        }else{
            if (nro_seq == ultimoNroSeqRcvEstadoSrvMulticast){
                EnviarPaquete("ACKM "+nro_seq);
            }else{
                int nro = nro_seq == 0 ? 0 : 1 ;
                EnviarPaquete("ACKM "+nro);
                ultimoNroSeqRcvEstadoSrvMulticast = nro;             
            }  
        }
    }
    //RECIVE
    public String ProcesarMensajeMulticast() throws IOException, ClienteException{
        String msg,mensaje;
        msg = RecibirPaqueteMulticast();       
        
        //System.out.println("Llegó por multicast " + msg);
        
        StringTokenizer tkn = new StringTokenizer(msg);
        if (tkn.hasMoreTokens()){
            String emisor, command; 
            command = tkn.nextToken();
            if (command.equals(RELAYED_MESSAGE)){
                if (tkn.hasMoreTokens()){
                    String seq = msg.substring(msg.trim().length()-1,msg.trim().length());
                    int nro_seq = Integer.parseInt(seq.trim()); //nrp seq recibido
                    
                    if (ultimoNroSeqRcvEstadoSrvMulticast == -1){
                        ultimoNroSeqRcvEstadoSrvMulticast = nro_seq == 0 ? 1 : 0;
                    }
                    
                    if (nro_seq == ultimoNroSeqRcvEstadoSrvMulticast){                        
                        EnviarACK(nro_seq, "MULTICAST");
                        return "";
                    }else{
                        emisor = tkn.nextToken();
                        mensaje = "<"+emisor+">" +" : ";
                        int indexNroSeq = msg.lastIndexOf(seq);
                        mensaje = mensaje + msg.substring(command.length()+emisor.length()+2, indexNroSeq -1);                  
                        EnviarACK(nro_seq, "MULTICAST");
                        return mensaje;
                    }                    
                }else{
                    return "";
                } 
            }else{
                return "";
            } 
        }else{
            return "";
        }        
    }
    
    public String ProcesarMensaje(String msg,JBoolean pidioListaUsr, List<String> listaU) throws ClienteException, InterruptedException, IOException{        
        String mensaje;
        
        //msg = RecibirPaquete();
        if (msg == null || msg.trim().equalsIgnoreCase("")){
            if (!mensajesRecibidos.isEmpty())
                mensajesRecibidos.remove(0);
            return "";
        }
        StringTokenizer tkn = new StringTokenizer(msg);        
        if (tkn.hasMoreTokens()){
            String emisor, command; 
            command = tkn.nextToken();    
            String seq = msg.substring(msg.trim().length()-1,msg.trim().length());
            int nro_seq = Integer.parseInt(seq.trim()); //nrp seq recibido
            if (command.equals(PRIVATE_MESSAGE)){
                if (tkn.hasMoreTokens()){
                    
                    //mutexNroSq.acquire();
                    if (nro_seq == ultimoNroSeqRcvEstadoServidor){                        
                        EnviarACK(nro_seq, "UNICAST");
                        if (!mensajesRecibidos.isEmpty())
                            mensajesRecibidos.remove(0);
                        return "";
                    }else{
                        emisor = tkn.nextToken();
                        mensaje = "PRIVADO DE : <"+emisor+"> : ";
                        int indexNroSeq = msg.lastIndexOf(seq);
                        mensaje = mensaje + msg.substring(command.length()+emisor.length()+2, indexNroSeq -1);                  
                        EnviarACK(nro_seq, "UNICAST");
                        if (!mensajesRecibidos.isEmpty())
                            mensajesRecibidos.remove(0);
                        return mensaje;
                    }
                    //mutexNroSq.release();
                }else{
                    if (!mensajesRecibidos.isEmpty())
                        mensajesRecibidos.remove(0);
                    return "";
                }
            }else if (command.equals(CONNECTED)){
                if (tkn.hasMoreTokens()){
                    
                    mensaje = msg.substring(command.length()+1, msg.length()); 
                    StringTokenizer tkn2 = new StringTokenizer(mensaje,"|");
                    listaU.clear();
                    pidioListaUsr.setPidioListaUsr(true); 
                    String usr;
                    while (tkn2.hasMoreTokens()){
                        usr = tkn2.nextToken();
                        if (!"".equalsIgnoreCase(usr.trim()) && !(usr.trim().equals("0") || usr.trim().equals("1")))
                            listaU.add(usr);
                    }                                       
                    EnviarACK(nro_seq, "UNICAST");
                    if (!mensajesRecibidos.isEmpty())
                        mensajesRecibidos.remove(0);
                    return mensaje;
                }else{
                    if (!mensajesRecibidos.isEmpty())
                        mensajesRecibidos.remove(0);
                    return "";
                }                
            }else if (command.equals(GOODBYE)){
                
                //EnviarACK(nro_seq, "UNICAST");    
                if (!mensajesRecibidos.isEmpty())
                    mensajesRecibidos.remove(0);
                return "GOODBYE";
            }else if (command.equals("ACK")){ //es un ACK
                
                mutexRecive.acquire();
                ultimoNroSeqEnvEstadoCliente = nro_seq; 
                mutexRecive.release();
                if (!mensajesRecibidos.isEmpty())
                    mensajesRecibidos.remove(0);
                return "";
            }else if (command.equals("ACKM")){                
                //System.out.println("Llegó ACKM");
                mutexRecive.acquire();
                ultimoNroSeqEnvEstadoClienteMulticast = nro_seq;    
                mutexRecive.release();
                if (!mensajesRecibidos.isEmpty())
                    mensajesRecibidos.remove(0);
                return "";
            }else{
                //comando incorrecto
                if (!mensajesRecibidos.isEmpty())
                    mensajesRecibidos.remove(0);
                return "";
            }                         
        }
        if (!mensajesRecibidos.isEmpty())
            mensajesRecibidos.remove(0);
        return "";
    }
    
    public void EnviarUnicast(String mensaje) throws ClienteException, IOException{
        StringTokenizer tkn = new StringTokenizer(mensaje);
        if (tkn.hasMoreTokens()){
            String command = tkn.nextToken();
            if (command.equals(PRIVATE_MESSAGE)){
                EnviarMensajePrivado(mensaje);
            }else if (command.equals(GET_CONNECTED)){
                EnviarMensajeListaConectados(mensaje);
            }
        }
    }
    
    
    public void EnviarMensajeListaConectados(String mensaje) throws ClienteException, IOException{
        mensaje = mensaje + " " + nroSeqEstadoCliente;           
        //Ahora tengo que recibir el ACK, sino reintentar                       
        EnvioMSGyEsperoACK(mensaje);  
        if (!mensajesAEnviar.isEmpty())
            mensajesAEnviar.remove(0);
    }
    
    
    public List<String> ListarConectados(){                
        listaDeUsuarios.clear();
        
        return listaDeUsuarios;
    }
    
    
    /////GETS Y SETS/////////////

    /**
     * @return the USUARIO
     */
    public String getUSUARIO() {
        return USUARIO;
    }

    /**
     * @return the estaLogueado
     */
    public boolean isEstaLogueado() {
        return estaLogueado;
    }

    /**
     * @param estaLogueado the estaLogueado to set
     */
    public void setEstaLogueado(boolean estaLogueado) {
        this.estaLogueado = estaLogueado;
    }

    /**
     * @return the IPMulticast
     */
    public InetAddress getIP_Multicast() {
        return IP_Multicast;
    }

    /**
     * @return the IPhost
     */
    public InetAddress getIP_Host() {
        return IP_Host;
    }

    /**
     * @return the PORTHOST
     */
    public int getPORT_Host() {
        return PORT_Host;
    }

    /**
     * @return the PORT_Multicast
     */
    public int getPORT_Multicast() {
        return PORT_Multicast;
    }

    /**
     * @param PORT_Multicast the PORT_Multicast to set
     */
    public void setPORT_Multicast(int PORT_Multicast) {
        this.PORT_Multicast = PORT_Multicast;
    }

    /**
     * @return the mensajesAEnviar
     */
    public List<String> getMensajesAEnviar() {
        return mensajesAEnviar;
    }
    public void AgregarMensajeAEnviar(String mensaje){
        mensajesAEnviar.add(mensaje);
    }
    /**
     * @param mensajesAEnviar the mensajesAEnviar to set
     */
    public void setMensajesAEnviar(List<String> mensajesAEnviar) {
        this.mensajesAEnviar = mensajesAEnviar;
    }

    /**
     * @return the mensajesRecibidos
     */
    public List<String> getMensajesRecibidos() {
        return mensajesRecibidos;
    }

    /**
     * @param mensajesRecibidos the mensajesRecibidos to set
     */
    public void setMensajesRecibidos(List<String> mensajesRecibidos) {
        this.mensajesRecibidos = mensajesRecibidos;
    }

    /**
     * @return the mensajesAEnviarMulticast
     */
    public List<String> getMensajesAEnviarMulticast() {
        return mensajesAEnviarMulticast;
    }
    public void AgregarMensajeAEnviarMulticast(String mensaje){
        mensajesAEnviarMulticast.add(mensaje);
    }
    /**
     * @param mensajesAEnviarMulticast the mensajesAEnviarMulticast to set
     */
    public void setMensajesAEnviarMulticast(List<String> mensajesAEnviarMulticast) {
        this.mensajesAEnviarMulticast = mensajesAEnviarMulticast;
    }

    /**
     * @return the tiempoDeEspera
     */
    public int getTiempoDeEspera() {
        return tiempoDeEspera;
    }

    /**
     * @param tiempoDeEspera the tiempoDeEspera to set
     */
    public void setTiempoDeEspera(int tiempoDeEspera) {
        this.tiempoDeEspera = tiempoDeEspera;
    }

    /**
     * @return the estaEnviandoUnicast
     */
    public boolean isEstaEnviandoUnicast() {
        return estaEnviandoUnicast;
    }

    /**
     * @param estaEnviandoUnicast the estaEnviandoUnicast to set
     */
    public void setEstaEnviandoUnicast(boolean estaEnviandoUnicast) {
        this.estaEnviandoUnicast = estaEnviandoUnicast;
    }

    /**
     * @return the estaEnviandoMulticast
     */
    public boolean isEstaEnviandoMulticast() {
        return estaEnviandoMulticast;
    }

    /**
     * @param estaEnviandoMulticast the estaEnviandoMulticast to set
     */
    public void setEstaEnviandoMulticast(boolean estaEnviandoMulticast) {
        this.estaEnviandoMulticast = estaEnviandoMulticast;
    }
           
}
