/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clienteudp;

import Excepciones.ClienteException;
import Negocio.ControladorConexion;
import java.io.IOException;

public class ThreadEnviarMensajes extends Thread{        
    
    @Override
    public void run() {        
        ControladorConexion ctrl = ControladorConexion.getinstance(); 
        //StringTokenizer command
        while (true) {                 
            try {         
                if (!ctrl.getMensajesAEnviar().isEmpty()){                    
                    ctrl.EnviarUnicast(ctrl.getMensajesAEnviar().get(0)); 
                }                                                 
            } catch (ClienteException ex) {
                //Logger.getLogger(ThreadRecibirMensaje.class.getName()).log(Level.SEVERE, null, ex);                
                //System.out.println("Error en thread Enviar Mensaje : " + ex.getMessage()); 
            } catch (IOException ex) {
                //Logger.getLogger(ThreadEnviarMensajes.class.getName()).log(Level.SEVERE, null, ex);
            }    
            try {
                sleep(50);
            } catch (InterruptedException ex) {
                //Logger.getLogger(ThreadEnviarMensajes.class.getName()).log(Level.SEVERE, null, ex);
                //System.out.println("Thread Enviar Mensaje Interrumpido."); 
            }
        }        
    }
    
}
