/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clienteudp;

import Excepciones.ClienteException;
import Negocio.Actividad;
import Negocio.ActividadUsuarios;
import Negocio.ControladorConexion;
import Negocio.JBoolean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ThreadProcesarMensajes extends Thread{
    private Actividad logger;
    private ActividadUsuarios loggerUsr;
    public ThreadProcesarMensajes(Actividad logger, ActividadUsuarios loggerUsr){
        this.logger = logger;
        this.loggerUsr = loggerUsr;
    }
    
    
    
    @Override
    public void run() {        
        ControladorConexion ctrl = ControladorConexion.getinstance();        
        String msg;
        JBoolean pidioListaUser = new JBoolean();
        List<String> listaU = new ArrayList<String>();
        while (true) {       
            //System.out.println("cosas"); 
            try {            
                if (!ctrl.getMensajesRecibidos().isEmpty()){ 
                    pidioListaUser.setPidioListaUsr(false); 
                    msg = ctrl.ProcesarMensaje(ctrl.getMensajesRecibidos().get(0), pidioListaUser, listaU); //esto se supone que recibe el mensaje y envía el ack correspondiente                                                
                    
                    if (pidioListaUser.pidioListaUsr()){
                        loggerUsr.logActionList(listaU); 
                    }else{
                        if (!msg.equals(""))
                            logger.logAction(msg); 
                    }
                }        
                //sleep((long) 0.150); 
                
            } catch (ClienteException ex) {
                //Logger.getLogger(ThreadRecibirMensaje.class.getName()).log(Level.SEVERE, null, ex);                
                //System.out.println("Procesar Mensaje : " + ex.getMessage()); 
            } catch (InterruptedException ex) {
                //Logger.getLogger(ThreadRecibirMensaje.class.getName()).log(Level.SEVERE, null, ex);
                //System.out.println("Procesar Mensaje Interrumpido."); 
            } catch (IOException ex) {
                //Logger.getLogger(ThreadRecibirMensaje.class.getName()).log(Level.SEVERE, null, ex);
                //System.out.println("Procesar Mensaje : " + ex.getMessage()); 
            }                                   
        }        
    }    
}
