/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clienteudp;

import Negocio.ControladorConexion;
import java.io.IOException;

public class ThreadRecibirMensaje extends Thread{
    @Override
     public void run() {        
         ControladorConexion ctrl = ControladorConexion.getinstance();                 
         while (true) {  
             try {
                 ctrl.RecibirPaquete();
             } catch (IOException ex) {
                 //Logger.getLogger(ThreadRecibirMensaje.class.getName()).log(Level.SEVERE, null, ex);
                 //System.out.println("Error en thread Recibir Mensaje : " + ex.getMessage()); 
             }
         }
     }
}
