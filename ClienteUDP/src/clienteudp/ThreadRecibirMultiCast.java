/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clienteudp;

import Excepciones.ClienteException;
import Negocio.Actividad;
import Negocio.ControladorConexion;
import java.io.IOException;

public class ThreadRecibirMultiCast extends Thread{
    
    private Actividad logger;
    
    public ThreadRecibirMultiCast(Actividad logger){
        this.logger = logger;
        
    }
    
    @Override
    public void run() {
        
        ControladorConexion ctrl = ControladorConexion.getinstance();        
        String msg;        
        while (true) {             
            try {                       
                msg = ctrl.ProcesarMensajeMulticast(); //esto se supone que recibe el mensaje y envía el ack correspondiente   
                if (!msg.equals(""))
                    logger.logAction(msg);                
                
            } catch (ClienteException ex) {
                //Logger.getLogger(ThreadRecibirMultiCast.class.getName()).log(Level.SEVERE, null, ex);                
            } catch (IOException ex) {
                //Logger.getLogger(ThreadRecibirMultiCast.class.getName()).log(Level.SEVERE, null, ex);
            }                                   
        }        
    }    
}
