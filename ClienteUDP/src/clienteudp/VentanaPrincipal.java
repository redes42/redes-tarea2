/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clienteudp;

import Excepciones.ClienteException;
import Negocio.Actividad;
import Negocio.ActividadUsuarios;
import Negocio.ControladorConexion;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class VentanaPrincipal extends javax.swing.JFrame {

    //private Thread hiloMulticast;
    /**
     * Creates new form VentanaPrincipal
     */
    ThreadRecibirMultiCast hiloRecibirMulticast;
    ThreadRecibirMensaje hiloRecibir;
    ThreadEnviarMensajes hiloEnviar;
    ThreadEnviarMensajesMulticast hiloEnviarMulticast;
    ThreadProcesarMensajes hiloProcesarMensajes;
    ControladorConexion ctrl;
    DefaultListModel modelo;
    TextChatLogger logger;
    ListChatLogger listLogger;
    boolean vieneDeUnLogout;
    
    public VentanaPrincipal(){
        initComponents();
        this.setLocationRelativeTo(null);
        ctrl = ControladorConexion.getinstance();
        vieneDeUnLogout = false;
        logger = new TextChatLogger(textChat);
        listLogger = new ListChatLogger(listaUsuarios);
        modelo = new DefaultListModel();
        modelo.clear();
        listaUsuarios.setModel(modelo);
        
        Action action = new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent e){                
                if (listaUsuarios.getSelectedIndices().length <= 0){                     
                    EnviarMulticast(ctrl);                    
                }else{                    
                    EnviarPrivado(ctrl);                    
                }
            }
        };
        Action actionLogin = new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent e){
                Login();
            }
        };
        SetearPantalla(false);
        textMensaje.addActionListener( action );
        textUser.addActionListener(actionLogin);
        textPuerto.addActionListener(actionLogin);
        textHost.addActionListener(actionLogin);        
        IniciarHilos();                
    }                       
    
    
    private void  IniciarHilos(){
        //Inicializo hilos multicast        
        hiloRecibirMulticast = new ThreadRecibirMultiCast(logger);        
        hiloRecibirMulticast.start();
        hiloEnviarMulticast = new ThreadEnviarMensajesMulticast();
        hiloEnviarMulticast.start();
        //Inicializo hilos de unicast
        hiloRecibir = new ThreadRecibirMensaje();
        hiloRecibir.start();
        hiloEnviar = new ThreadEnviarMensajes();
        hiloEnviar.start();
        hiloProcesarMensajes = new ThreadProcesarMensajes(logger, listLogger);
        hiloProcesarMensajes.start();
    }
    
    
    public class TextChatLogger implements Actividad{
        private final JTextPane destino;

        public TextChatLogger(JTextPane target) {
            this.destino = target;
        }

        public void logAction(final String message){
            SwingUtilities.invokeLater(new Runnable(){
                @Override
                public void run() {
                    
                    StyledDocument doc;
                    doc = destino.getStyledDocument();
                    Style style = destino.addStyle("Estilo", null);
                    
                    
                    if (message.contains("PRIVADO DE : <")){
                        StyleConstants.setForeground(style, Color.BLUE);
                    }else{
                        StyleConstants.setForeground(style, Color.black);
                    }
                    
                    try {
                        doc.insertString(doc.getLength(), message+"\n", style);
                    } catch (BadLocationException ex) {
                    } 
                    if (message.trim().equals("GOODBYE")){
                        Logout(true);
                    }                                        
                }
            });
        }
    }            
    public class ListChatLogger implements ActividadUsuarios{
        private final JList destino;

        public ListChatLogger(JList target) {
            this.destino = target;
        }

        public void logActionList(final List<String> message){
            SwingUtilities.invokeLater(new Runnable(){
                @Override
                public void run() {
                    modelo = new DefaultListModel();
                    modelo.removeAllElements();
                    if (message.size() > 0){
                        for(int i = 0; i < message.size(); i++){
                            modelo.addElement(message.get(i)); 
                        }
                        destino.setModel(modelo);
                    }
                }
            });
        }
    } 
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelChat = new javax.swing.JPanel();
        textMensaje = new javax.swing.JTextField();
        botonEnviar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        listaUsuarios = new javax.swing.JList();
        labelChat = new javax.swing.JLabel();
        botonActualizaUsr = new javax.swing.JButton();
        labelConexion = new javax.swing.JLabel();
        botonLimpiarChat = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        textChat = new javax.swing.JTextPane();
        panelLogin = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        textHost = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        textPuerto = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        textUser = new javax.swing.JTextField();
        botonLogin = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        botonEnviar.setText(">> Enviar");
        botonEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEnviarActionPerformed(evt);
            }
        });

        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        listaUsuarios.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listaUsuarios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listaUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listaUsuariosMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(listaUsuarios);

        labelChat.setText("DESCONECTADO");

        botonActualizaUsr.setText("Actualizar ");
        botonActualizaUsr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonActualizaUsrActionPerformed(evt);
            }
        });

        labelConexion.setBackground(new java.awt.Color(255, 0, 0));
        labelConexion.setOpaque(true);

        botonLimpiarChat.setText("Limpiar chat");
        botonLimpiarChat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonLimpiarChatActionPerformed(evt);
            }
        });

        jScrollPane4.setViewportView(textChat);

        javax.swing.GroupLayout panelChatLayout = new javax.swing.GroupLayout(panelChat);
        panelChat.setLayout(panelChatLayout);
        panelChatLayout.setHorizontalGroup(
            panelChatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelChatLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelChatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelChatLayout.createSequentialGroup()
                        .addComponent(textMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                        .addComponent(botonEnviar))
                    .addGroup(panelChatLayout.createSequentialGroup()
                        .addComponent(labelConexion, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelChat, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botonLimpiarChat))
                    .addComponent(jScrollPane4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelChatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                    .addComponent(botonActualizaUsr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelChatLayout.setVerticalGroup(
            panelChatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelChatLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelChatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelConexion, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelChat)
                    .addGroup(panelChatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(botonActualizaUsr)
                        .addComponent(botonLimpiarChat)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelChatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 217, Short.MAX_VALUE)
                    .addComponent(jScrollPane4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelChatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonEnviar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setText("Host:");

        textHost.setText("164.73.44.");

        jLabel2.setText("Puerto:");

        textPuerto.setText("54321");

        jLabel3.setText("Usuario :");

        textUser.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textUserKeyTyped(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                textUserKeyReleased(evt);
            }
        });

        botonLogin.setText("Login");
        botonLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonLoginActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelLoginLayout = new javax.swing.GroupLayout(panelLogin);
        panelLogin.setLayout(panelLoginLayout);
        panelLoginLayout.setHorizontalGroup(
            panelLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLoginLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(botonLogin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelLoginLayout.createSequentialGroup()
                        .addGroup(panelLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(textHost, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textPuerto, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textUser, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        panelLoginLayout.setVerticalGroup(
            panelLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLoginLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textHost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textUser)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addComponent(botonLogin)
                .addGap(120, 120, 120))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelChat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelLogin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelChat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void EnviarPrivado(ControladorConexion ctrl){
        if (!textMensaje.getText().trim().equals("")){
            if (listaUsuarios.getSelectedIndex() != -1){
                String nombre = (String) modelo.getElementAt(listaUsuarios.getSelectedIndex());
                //ctrl.EnviarMensajePrivado(textMensaje.getText(), nombre);
                if (!nombre.trim().equalsIgnoreCase(ctrl.getUSUARIO().trim())){
                    String mensaje = "PRIVATE_MESSAGE " + nombre+ " " + textMensaje.getText();
                    //textChat.setText(String.format("%s%s%n", textChat.getText(), "PRIVADO PARA : <"+nombre+"> : "+textMensaje.getText()));                 
                    String message = String.format("%s%n", "PRIVADO PARA : <"+nombre+"> : "+textMensaje.getText());
                    StyledDocument doc;
                    doc = textChat.getStyledDocument();
                    Style style = textChat.addStyle("Estilo", null);
                    StyleConstants.setForeground(style, Color.black);               
                    try {
                        doc.insertString(doc.getLength(), message, style);
                    } catch (BadLocationException ex) {
                    }

                    ctrl.AgregarMensajeAEnviar(mensaje); 
                }else{
                    JOptionPane.showMessageDialog(this, "No se puede enviar mensaje privado a uno mismo.");
                }                                
                textMensaje.setText(""); 
            }
        }
    }
    private void EnviarMulticast(ControladorConexion ctrl) {
        if (!textMensaje.getText().trim().equals("")){
            String mensaje = "MESSAGE" + " " + textMensaje.getText();
            ctrl.AgregarMensajeAEnviarMulticast(mensaje);
            //ctrl.EnviarMensajeMulticast(textMensaje.getText());
            //textChat.setText(String.format("%s%s%n", textChat.getText(), "<"+ctrl.getUSUARIO()+"> Dice : "+textMensaje.getText()));             
            textMensaje.setText(""); 
        }
    }
    private void botonEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEnviarActionPerformed
                                
        if (listaUsuarios.getSelectedIndices().length <= 0){//es multi
            EnviarMulticast(ctrl);
        }else{ //es privado
            EnviarPrivado(ctrl);
        } 
        
    }//GEN-LAST:event_botonEnviarActionPerformed
    public void SetearPantalla(boolean conectado){
        if (conectado){
            labelConexion.setBackground(Color.GREEN);
            labelChat.setText("CONECTADO : <" + ctrl.getUSUARIO() + ">");
            botonLogin.setText("Logout");
        }else{
            labelConexion.setBackground(Color.RED);
            labelChat.setText("DESCONECTADO");
            botonLogin.setText("Login");
            modelo.removeAllElements();
        }
        textHost.setEnabled(!conectado);
        textPuerto.setEnabled(!conectado);
        textUser.setEnabled(!conectado);
        botonActualizaUsr.setVisible(conectado);
        botonEnviar.setEnabled(conectado);
        textChat.setText("");
        textMensaje.setText(""); 
    }
    private void InterrumpirHilos(){  
        hiloEnviar.stop();
        hiloEnviarMulticast.stop();
        hiloRecibirMulticast.stop();         
        hiloProcesarMensajes.stop();
        hiloRecibir.stop();                               
    }
    private void DestruyoHilos(){                  
        hiloEnviar.interrupt();
        hiloEnviarMulticast.interrupt();
        hiloRecibirMulticast.interrupt();         
        hiloProcesarMensajes.interrupt();
        hiloRecibir.interrupt();
    }
    private void Logout(boolean meSacoElServer){
        try {
            if (meSacoElServer){
                ctrl.Logout();                                     
                SetearPantalla(false);
                vieneDeUnLogout=true;
                InterrumpirHilos();
                ctrl.CerrarSockets();
                DestruyoHilos();
                textChat.setText("Desconectado del Servidor por inactividad...");
            }else{
                if (!ctrl.isEstaEnviandoUnicast() && !ctrl.isEstaEnviandoMulticast()){
                    ctrl.Logout();                
                    vieneDeUnLogout = true;
                    SetearPantalla(false); 
                    InterrumpirHilos();
                    ctrl.CerrarSockets();
                    DestruyoHilos();
                }else{
                    int result = JOptionPane.showConfirmDialog(this, "Su mensaje no ha sido confirmado por el servidor. ¿Desea realizar el Logout de todas formas? Sino reintente en unos segundos.");
                    if (result == JOptionPane.OK_OPTION){
                        ctrl.Logout();                                     
                        SetearPantalla(false);
                        vieneDeUnLogout=true;
                        InterrumpirHilos();
                        ctrl.CerrarSockets();
                        DestruyoHilos();
                    }
                } 
            }
        } catch (ClienteException ex) {
            StringTokenizer tkn = new StringTokenizer(ex.getMessage());
            if (tkn.hasMoreTokens()){
                if ("TIMEOUT".equalsIgnoreCase(tkn.nextToken())){      
                    ctrl.CerrarSockets();
                    vieneDeUnLogout = true;
                    SetearPantalla(false);  
                    InterrumpirHilos();
                    ctrl.CerrarSockets();
                    DestruyoHilos();
                    JOptionPane.showMessageDialog(this, "El servidor no responde. Intente más tarde.");
                }else{
                    JOptionPane.showMessageDialog(this, ex.getMessage());
                }
            }                        
        } catch (IOException ex) {
            //Logger.getLogger(VentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        } 
    }
    private void Login(){
        
        try {            
            if (vieneDeUnLogout){
                ctrl.Init(); 
                IniciarHilos();
            }            
            ctrl.Login(textHost.getText(), textPuerto.getText(), textUser.getText());  
            vieneDeUnLogout = false;
            //ctrl.AgregarMensajeAEnviar("GET_CONNECTED"); 
            SetearPantalla(true);
        } catch (ClienteException ex) { 
            StringTokenizer tkn = new StringTokenizer(ex.getMessage());
            if (tkn.hasMoreTokens()){
                if ("TIMEOUT".equalsIgnoreCase(tkn.nextToken())){      
                    //ctrl.CerrarSockets();
                    //vieneDeUnLogout = true;
                    SetearPantalla(false);  
                    JOptionPane.showMessageDialog(this, "El servidor no responde. Intente más tarde.");
                }else{
                    JOptionPane.showMessageDialog(this, ex.getMessage());
                }
            }
                                    
        } catch (IOException ex) {
            //Logger.getLogger(VentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage()); 
        }        
        //si llegó hasta acá, es porque en el método Login recibió el correspondiente ACK         
    }
    
    private void botonLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonLoginActionPerformed
        if (ctrl.isEstaLogueado()) {
            Logout(false);
        }else{
            Login();
        }                      
    }//GEN-LAST:event_botonLoginActionPerformed

    private void botonActualizaUsrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonActualizaUsrActionPerformed
               
        ctrl.AgregarMensajeAEnviar("GET_CONNECTED"); 
                            
    }//GEN-LAST:event_botonActualizaUsrActionPerformed

    private void botonLimpiarChatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonLimpiarChatActionPerformed
        // TODO add your handling code here:
        textChat.setText("");
    }//GEN-LAST:event_botonLimpiarChatActionPerformed

    private void listaUsuariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listaUsuariosMouseClicked
        // TODO add your handling code here:
        
    }//GEN-LAST:event_listaUsuariosMouseClicked

    private void textUserKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textUserKeyReleased
        // TODO add your handling code here:       
    }//GEN-LAST:event_textUserKeyReleased

    private void textUserKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textUserKeyTyped
        // TODO add your handling code here:
        char caracter = evt.getKeyChar();
        if((caracter == KeyEvent.VK_SPACE)){
            evt.consume();
        }
    }//GEN-LAST:event_textUserKeyTyped
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new VentanaPrincipal().setVisible(true);
                    /*InetAddress IP=InetAddress.getLocalHost();
                    System.out.println("IP of my system is := "+IP.getHostAddress());*/
                } catch (Exception ex) {
                    Logger.getLogger(VentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonActualizaUsr;
    private javax.swing.JButton botonEnviar;
    private javax.swing.JButton botonLimpiarChat;
    private javax.swing.JButton botonLogin;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel labelChat;
    private javax.swing.JLabel labelConexion;
    private javax.swing.JList listaUsuarios;
    private javax.swing.JPanel panelChat;
    private javax.swing.JPanel panelLogin;
    private javax.swing.JTextPane textChat;
    private javax.swing.JTextField textHost;
    private javax.swing.JTextField textMensaje;
    private javax.swing.JTextField textPuerto;
    private javax.swing.JTextField textUser;
    // End of variables declaration//GEN-END:variables

    

    
}
