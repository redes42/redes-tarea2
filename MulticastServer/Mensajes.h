#ifndef MENSAJES_H
#define	MENSAJES_H
#include <string>
#include <iostream>
#include <stdio.h>

using namespace std;

class Mensaje{
    public:
    Mensaje(string Msj, string usr, Mensaje *sig = NULL)
    {
       Texto = Msj;           // Mensaje que se desea enviar
       NombreUsuario = usr;     // Usuario Receptor  
       next = sig;
    }
    
    string Texto;
    string NombreUsuario;
    Mensaje* next;
    
   friend class Mensajes;
    
};

typedef Mensaje *mensaje;
 
class Mensajes {
   public:
    int tope;
    mensaje primero;
    mensaje ultimo;
    Mensajes();
    ~Mensajes();
    
    void Insertar(string Msj, string usr);
    void Borrar();
    bool ListaVacia() { return primero == NULL; } 
    
    Mensaje* MensajeActual() { return primero; }
    

};


#endif	