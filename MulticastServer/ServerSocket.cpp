// Implementation of the ServerSocket class

#include "ServerSocket.h"
#include "SocketException.h"


ServerSocket::ServerSocket ( string host, int port )
{
  if ( ! Socket::create() )
    {
      throw SocketException ( "No se pudo crear el socket." );
    }

  if ( ! Socket::bind (host, port ) )
    {
      throw SocketException ( "No se pudo hace el bind con el puerto." );
    }
   m_sock = Socket::getNumeroSocket();
}

ServerSocket::~ServerSocket()
{
}

const ServerSocket& ServerSocket:: write ( const std::string& s, string ip, int port)
{
  if ( ! Socket::sendToPrivate ( s , ip, port) )
    {
      throw SocketException ( "No se pudo escribir en el socket." );
    }

  return *this;

}
int ServerSocket::GetNumeroSocket(){
    return m_sock;
}


const ServerSocket& ServerSocket:: read ( std::string& s, string& ip, int& port ) const
{
  if ( ! Socket::recvfrom ( s, ip, port ) )
    {
      throw SocketException ( "No se pudo leer del socket." );
    }

  return *this;
}
