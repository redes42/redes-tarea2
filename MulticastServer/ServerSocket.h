// Definition of the ServerSocket class

#ifndef ServerSocket_class
#define ServerSocket_class

#include "Socket.h"


class ServerSocket : private Socket
{
 public:

  ServerSocket ( string host, int port );
  ServerSocket (){};
  virtual ~ServerSocket();

  const ServerSocket& write ( const std::string& , string ip, int port);
  const ServerSocket& read ( std::string&, string& ip, int& port ) const;
  int GetNumeroSocket();
  private:
  int m_sock;
       
};
#endif
