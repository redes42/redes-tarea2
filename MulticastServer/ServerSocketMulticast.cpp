// Implementation of the ServerSocket class

#include "ServerSocketMulticast.h"
#include "SocketException.h"



ServerSocketMulticast::~ServerSocketMulticast()
{
}


ServerSocketMulticast::ServerSocketMulticast (string host, int port)
{
  if ( ! Socket::createMulticast() )
    {
      throw SocketException ( "No se pudo crear el socket." );
    }

  if ( ! Socket::bindMulticast (host, port) )
    {
      throw SocketException ( "No se pudo hacer el bind con el puerto." );
    }
}


const ServerSocketMulticast& ServerSocketMulticast:: write ( const std::string& s ) const
{
  if ( ! Socket::sendTo ( s ) )
    {
      throw SocketException ( "No se pudo escribir en el socket." );
    }

  return *this;

}


const ServerSocketMulticast& ServerSocketMulticast:: read ( std::string& s, string& ip, int& port ) const
{
  if ( ! Socket::recvfrom ( s, ip, port ) )
    {
      throw SocketException ( "No se pudo leer en el socket." );
    }

  return *this;
}
