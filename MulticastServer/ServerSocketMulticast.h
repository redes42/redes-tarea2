// Definition of the ServerSocket class

#ifndef ServerSocketMulticast_class
#define ServerSocketMulticast_class

#include "Socket.h"


class ServerSocketMulticast : private Socket
{
 public:

  ServerSocketMulticast(string host, int port);
  ServerSocketMulticast (){};
  virtual ~ServerSocketMulticast();

  const ServerSocketMulticast& write ( const std::string& ) const;
  const ServerSocketMulticast& read ( std::string&, string& ip, int& port ) const;


};


#endif
