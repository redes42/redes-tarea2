// Implementation of the Socket class.


#include "Socket.h"
#include "string.h"
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <stdio.h>

using namespace std; 


Socket::Socket() :
  m_sock ( -1 )
{

  memset ( &m_addr,
	   0,
	   sizeof ( m_addr ) );
  memset ( &m_addr_private,
	   0,
	   sizeof ( m_addr_private ) );

}

Socket::~Socket()
{
  if ( is_valid() )
    ::close ( m_sock );
}

bool Socket::create()
{
  m_sock = socket ( AF_INET,
                    SOCK_DGRAM,
		    0 );

  if ( ! is_valid() )
    return false;
  
  int on = 1;
  
  if ( setsockopt ( m_sock, SOL_SOCKET, SO_REUSEADDR, ( const char* ) &on, sizeof ( on ) ) == -1 ) {   
    return false;
  }


  return true;

}

bool Socket::createMulticast()
{
  m_sock = socket ( AF_INET,
                    SOCK_DGRAM,
		    0 );
  if ( ! is_valid() )
    return false;

   
  char loopch = 0;
  if(setsockopt(m_sock, IPPROTO_IP, IP_MULTICAST_LOOP, (char *)&loopch, sizeof(loopch)) < 0)
  {

    //std::cout << "Setting IP_MULTICAST_LOOP error";

    close(m_sock);

    return false;

  }
  else
    return true;
}

bool Socket::bind ( const string& host, const int port )
{

  if ( ! is_valid() )
    {
      return false;
    }



  m_addr.sin_family = AF_INET;
  m_addr.sin_addr.s_addr = inet_addr( host.c_str() );  
  m_addr.sin_port = htons ( port );

  int bind_return = ::bind ( m_sock,
			     ( struct sockaddr * ) &m_addr,
			     sizeof ( m_addr ) );


  if ( bind_return == -1 )
    {
      //cout<< "El error en el BIND es = ";
      printf ("Error al abrir un archivo inexistente.ent: %s\n",strerror(errno));
      return false;
    }

  return true;
}

bool Socket::bindMulticast (const string& host, const int port )
{

  if ( ! is_valid() )
    {
      return false;
    }
  
  /* Initialize the group m_addr structure with a */

  /* group address of 225.5.4.42 and port 54321. */
  
  memset((char *) &m_addr, 0, sizeof(m_addr));

  m_addr.sin_family = AF_INET;
  m_addr.sin_addr.s_addr = inet_addr(host.c_str());
  m_addr.sin_port = htons(port);

  int bind_return = ::bind ( m_sock,
			     ( struct sockaddr * ) &m_addr,
			     sizeof ( m_addr ) );


  if ( bind_return == -1 )
    {
      return false;
    }

  return true;
}


bool Socket::sendTo ( const std::string s ) const
{
  char buffer [ MAXRECV + 1 ];
  memset ( buffer, 0, MAXRECV + 1 );
  strcpy(buffer,s.c_str());
  size_t buffer_length = sizeof ( buffer );
  socklen_t addr_length = sizeof ( m_addr );
  ssize_t status = ::sendto(m_sock,buffer, buffer_length, 0, (struct sockaddr *)&m_addr,addr_length );
  if ( status == -1 )
    {
      return false;
    }
  else
    {
      return true;
    }
}
bool Socket::sendToPrivate ( std::string s , string ip, int port) 
{
  char buffer [ MAXRECV + 1 ];
  memset ( buffer, 0, MAXRECV + 1 );
  strcpy(buffer,s.c_str());
  size_t buffer_length = sizeof ( buffer );
  

  m_addr.sin_family = AF_INET;
  m_addr.sin_addr.s_addr = inet_addr(ip.c_str());
  m_addr.sin_port = htons(port);
  
  socklen_t addr_length = sizeof ( m_addr );
  ssize_t status = ::sendto(m_sock,buffer, buffer_length, 0, (struct sockaddr *)&m_addr,addr_length );
  if ( status == -1 )
    {
      //cout<< "El error en el sendToPrivate es = ";
      printf ("Error al abrir un archivo inexistente.ent: %s\n",strerror(errno));
      return false;
    }
  else
    {
      return true;
    }
}


int Socket::recvfrom ( std::string& s, string& ip, int &port ) const
{
   
  char buffer [ MAXRECV + 1 ];
  memset ( buffer, 0, MAXRECV + 1 );
  s = "";
  
  size_t buffer_length = sizeof ( buffer );
  socklen_t addr_length = sizeof ( m_addr );
  ssize_t status = ::recvfrom(m_sock,buffer,buffer_length ,0,( struct sockaddr * ) &m_addr,&addr_length );
  ip = inet_ntoa(m_addr.sin_addr);
  port = ntohs(m_addr.sin_port);
  
 
  if ( status == -1 )
    {
      std::cout << "status == -1   errno == " << errno << "  in Socket::recv\n";
      return 0;
    }
  else if ( status == 0 )
    {
      return 0;
    }
  else
    {
      s = buffer;
      return status;
    }
}
int Socket::getNumeroSocket(){
    return m_sock;
}

void Socket::set_non_blocking ( const bool b )
{

  int opts;

  opts = fcntl ( m_sock,
		 F_GETFL );

  if ( opts < 0 )
    {
      return;
    }

  if ( b )
    opts = ( opts | O_NONBLOCK );
  else
    opts = ( opts & ~O_NONBLOCK );

  fcntl ( m_sock,
	  F_SETFL,opts );

}