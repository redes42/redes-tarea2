// Definition of the Socket class

#ifndef Socket_class
#define Socket_class


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <arpa/inet.h>

using namespace std; 
const int MAXHOSTNAME = 200;
const int MAXCONNECTIONS = 5;
const int MAXRECV = 500;

class Socket
{
 public:
  Socket();
  virtual ~Socket();

  // Server initialization
  bool create();
  int getNumeroSocket();
  bool createMulticast();
  bool bind ( const string& host, const int port );
  bool bindMulticast (const string& host, const int port);
  
  // Data Transimission
  bool sendTo ( const std::string ) const;
  bool sendToPrivate (  std::string s , string ip, int port);
  int recvfrom ( std::string& , string& ip, int& port) const;


  void set_non_blocking ( const bool );

  bool is_valid() const { return m_sock != -1; }

 private:
     
  int m_sock;
  sockaddr_in m_addr;
  sockaddr_in m_addr_private;
  
};


#endif