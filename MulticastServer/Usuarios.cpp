#include "Usuarios.h"
#include "Mensajes.h"
#include "ServerSocket.h"
#include "string.h"
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <string>
#include <iostream>
#include <stdio.h>
#include <memory>

Usuarios::~Usuarios() {
    Usuario* aux;

    while (primero) {
        aux = primero;
        primero = primero->siguiente;
        delete aux;
    }
    actual = NULL;
}

bool Usuarios::Insertar(string ip, int port, string usr) {
    usuario anterior;
    Usuarios usuarios;

    string ipNum;
    ipNum = ObtenerIP(usr);

    if (ipNum == "") {
        // Si la lista está vacía
        if (ListaVacia() || primero->NombreUsuario > usr) {
            // Asignamos a lista un nuevo nodo de valor ip, usr y
            // cuyo siguiente elemento es la lista actual                    
            primero = new Usuario(ip, port, usr, primero);
        } else {
            // Buscar el nodo de valor menor a v 
            anterior = primero;
            // Avanzamos hasta el último elemento o hasta que el siguiente tenga 
            // un valor mayor que v 
            while (anterior->siguiente && anterior->siguiente->NombreUsuario <= usr)
                anterior = anterior->siguiente;
            // Creamos un nuevo nodo después del nodo anterior, y cuyo siguiente
            // es el siguiente del anterior
            anterior->siguiente = new Usuario(ip, port, usr, anterior->siguiente);
        }
        tope = tope + 1;
        return true;
    } else
        return false;
}

void Usuarios::Borrar(string ip, string usr) {
    usuario anterior, actual;

    actual = primero;
    anterior = NULL;
    int comp1 = actual->NombreUsuario.compare(usr);
    int comp2 = actual->IpNumero.compare(ip);
    while (actual && comp1 < 0 and comp2 != 0) {
        anterior = actual;
        actual = actual->siguiente;
    }
    if (!actual || comp1 != 0) return;
    else { // Borrar el nodo 
        if (!anterior) // Primer elemento 
            primero = actual->siguiente;
        else // un elemento cualquiera
            anterior->siguiente = actual->siguiente;
        delete actual;
        tope = tope - 1;
    }

}

string Usuarios::Mostrar(string usr) {
    Usuario *aux;
    string lista = "";
    usuario actual;
    actual = primero;
    string nombreUsr;

    string MayusUsr = usr;
    convert(MayusUsr);
    if (actual != NULL) {
        aux = primero;

        while (aux) {
            nombreUsr = aux->NombreUsuario;
            convert(nombreUsr);
            //if (nombreUsr.compare(MayusUsr) != 0){
            lista += aux->NombreUsuario + "|";
            //}
            aux = aux->siguiente;
        }
    }
    return lista;
}

void Usuarios::Siguiente() {
    if (actual) actual = actual->siguiente;
}

void Usuarios::Primero() {
    actual = primero;
}

void Usuarios::Ultimo() {
    actual = primero;
    if (!ListaVacia())
        while (actual->siguiente) Siguiente();
}

Usuario* Usuarios::Buscar(string usr) {
    usuario actual;
    actual = primero;

    if (actual != NULL) {
        int cnt = 0;

        string nombreUsr;
        nombreUsr = actual->NombreUsuario;
        convert(nombreUsr);
        string MayusUsr;
        MayusUsr = usr;

        convert(MayusUsr);

        while (cnt < tope and nombreUsr.compare(MayusUsr) != 0) {
            actual = actual->siguiente;
            cnt++;
            if (cnt < tope) {
                nombreUsr = actual->NombreUsuario;
                convert(nombreUsr);
            }
        }

        if (!actual)
            return NULL;
        else {
            return actual;
        }
    } else
        return NULL;
}

Usuario* Usuarios::BuscarIP(string ip) {
    usuario actual;
    actual = primero;

    if (actual != NULL) {
        int cnt = 0;

        string nombreUsr;
        nombreUsr = actual->IpNumero;
        convert(nombreUsr);
        string MayusUsr;
        MayusUsr = ip;

        convert(MayusUsr);

        while (cnt < tope and nombreUsr.compare(MayusUsr) != 0) {
            actual = actual->siguiente;
            cnt++;
            if (cnt < tope) {
                nombreUsr = actual->IpNumero;
                convert(nombreUsr);
            }
        }

        if (!actual)
            return NULL;
        else {
            return actual;
        }
    } else
        return NULL;
}

void Usuarios::AgregarMensaje(string msg, string usrReceptor) {

    Usuario* usr = Buscar(usrReceptor);

    if (usr != NULL) { // Encontre el usuario, Agrego el mensaje a la lista de mensajes
        Mensajes* m = usr->mensa;
        m->Insertar(msg, usrReceptor);
    }
}

void Usuarios::BorrarMensaje(string usrEmisor) {

    Usuario* usr = Buscar(usrEmisor);

    if (usr != NULL) { // Encontre el usuario, Borro el primer mensaje del usuario
        Mensajes* m = usr->mensa;
        m->Borrar();
    }
}

Mensaje* Usuarios::ObtenerMensaje(string usrEmisor) {

    Usuario* usr = Buscar(usrEmisor);
    if (usr != NULL) { // Encontre el usuario, Borro el primer mensaje del usuario
        Mensajes* m = usr->mensa;
        if (!m->ListaVacia())
            return m->primero;
        else
            return NULL;
    } else
        return NULL;
}

int Usuarios::ObtenerPuerto(string usrPort) {

    Usuario* usr = Buscar(usrPort);

    if (usr != NULL) { // Encontre el usuario, Borro el primer mensaje del usuario
        return usr->Puerto;
    } else
        return -1;

}

string Usuarios::ObtenerIP(string usrIP) {

    Usuario* usr = Buscar(usrIP);
    if (usr != NULL) { // Encontre el usuario, Borro el primer mensaje del usuario
        return actual->IpNumero;
    } else
        return "";
}

string Usuarios::ObtenerNombreIP(string ip) {

    Usuario* usr = BuscarIP(ip);
    if (usr != NULL) {
        return usr->NombreUsuario;
    } else
        return "";
}

int Usuarios::ObtenerNroSecuenciaRecibido(string usr) {
    Usuario* user = Buscar(usr);
    if (user != NULL)
        return user->nroSecuenciaRecibido;
    else
        return -1;

}

int Usuarios::ObtenerUltimoACKRecibido(string usr) {
    Usuario* user = Buscar(usr);
    if (user != NULL)
        return user->ultimoAckRecibido;
    else
        return -1;

}

int Usuarios::ObtenerNroSecuenciaEnviado(string usr) {
    Usuario* user = Buscar(usr);
    if (user != NULL)
        return user->nroSecuenciaEnviado;
    else
        return -1;

}

int Usuarios::ObtenerUltimoACKEnviado(string usr) {
    Usuario* user = Buscar(usr);
    if (user != NULL)
        return user->ultimoAckEnviado;
    else
        return -1;
}

int Usuarios::ObtenerNroSecuenciaMRecibido(string usr) {
    Usuario* user = Buscar(usr);
    if (user != NULL)
        return user->nroSecuenciaMRecibido;
    else
        return -1;

}

int Usuarios::ObtenerUltimoAckMRecibido(string usr) {
    Usuario* user = Buscar(usr);
    if (user != NULL)
        return user->ultimoAckMRecibido;
    else
        return -1;

}

int Usuarios::ObtenerNroSecuenciaMEnviado(string usr) {
    Usuario* user = Buscar(usr);
    if (user != NULL)
        return user->nroSecuenciaMEnviado;
    else
        return -1;

}

int Usuarios::ObtenerUltimoAckMEnviado(string usr) {
    Usuario* user = Buscar(usr);
    if (user != NULL)
        return user->ultimoAckMEnviado;
    else
        return -1;
}

void Usuarios::setUltimoAckEnviado(string usr) {
    Usuario* user = Buscar(usr);
    if (user->ultimoAckEnviado == 0)
        user->ultimoAckEnviado = 1;
    else
        user->ultimoAckEnviado = 0;
}

void Usuarios::setNroSecuenciaEnviado(string usr) {
    Usuario* user = Buscar(usr);
    if (user->nroSecuenciaEnviado == 0)
        user->nroSecuenciaEnviado = 1;
    else
        user->nroSecuenciaEnviado = 0;
}

void Usuarios::setAckRecibido(string usr) {
    Usuario* user = Buscar(usr);
    if (user->ultimoAckRecibido == 0)
        user->ultimoAckRecibido = 1;
    else
        user->ultimoAckRecibido = 0;
}

void Usuarios::setNroSecuenciaRecibido(string usr) {
    Usuario* user = Buscar(usr);
    if (user->nroSecuenciaRecibido == 0)
        user->nroSecuenciaRecibido = 1;
    else
        user->nroSecuenciaRecibido = 0;
}

void Usuarios::setUltimoAckMEnviado(string usr) {
    Usuario* user = Buscar(usr);
    if (user->ultimoAckMEnviado == 0)
        user->ultimoAckMEnviado = 1;
    else
        user->ultimoAckMEnviado = 0;
}

void Usuarios::setNroSecuenciaMEnviado(string usr) {
    Usuario* user = Buscar(usr);
    if (user->nroSecuenciaMEnviado == 0)
        user->nroSecuenciaMEnviado = 1;
    else
        user->nroSecuenciaMEnviado = 0;
}

void Usuarios::setUltimoAckMRecibido(string usr) {
    Usuario* user = Buscar(usr);
    if (user->ultimoAckMRecibido == 0)
        user->ultimoAckMRecibido = 1;
    else
        user->ultimoAckMRecibido = 0;
}

void Usuarios::setNroSecuenciaMRecibido(string usr) {
    Usuario* user = Buscar(usr);
    if (user->nroSecuenciaMRecibido == 0)
        user->nroSecuenciaMRecibido = 1;
    else
        user->nroSecuenciaMRecibido = 0;
}

bool Usuarios::llegoMensajeMulticast(int nroSecuencia) {
    usuario actual;
    actual = primero;

    if (actual != NULL) {
        bool llegoMensaje = true;
        while (actual != NULL && llegoMensaje) {
            if (actual->ultimoAckMRecibido != nroSecuencia) {
                llegoMensaje = false;
            }
            actual = actual->siguiente;
        }
        return llegoMensaje;
    }

    return true;
}

void Usuarios::setUltimoACKMRecibido(string usr, int ACKM){
    Usuario* user = Buscar(usr);
    if (user != NULL){
        user->ultimoAckMRecibido = ACKM;
    }
     
}

bool Usuarios::eliminarUsuario(string usr){
    Usuario* aux = primero;
    Usuario* antes = NULL;
    while(aux->NombreUsuario != usr) {
        antes = aux;
        aux = aux->siguiente;
   }
    if (aux != NULL){
        if (antes != NULL){
            antes->siguiente = aux->siguiente;
            aux->siguiente = NULL;
        }
        else{
            primero = primero->siguiente;
            aux->siguiente = NULL;
        }
        // Ahora tengo que eliminar los mensajes
        aux->mensa->~Mensajes();
        tope = tope -1;
        delete aux;
        return true;
    }
    else
        return false;
}

void Usuarios::convert(string& s) {
    for (int i = 0; i < s.length(); i++) {
        s[i] = toupper(s[i]);
    }
}