#ifndef USUARIOS_H
#define	USUARIOS_H
#include <string>
#include <iostream>
#include <stdio.h>
#include "Mensajes.h"
#include "ServerSocket.h"

using namespace std;

class Usuario {
public:
    Usuario(string ip, int port, string usr, Usuario *sig = NULL) {
        IpNumero = ip;
        Puerto = port;
        NombreUsuario = usr;
        siguiente = sig;
        mensa = new Mensajes();
        
        nroSecuenciaRecibido = 0;
        ultimoAckRecibido = 1;
        nroSecuenciaEnviado = 0;
        ultimoAckEnviado = 1;
        
        nroSecuenciaMRecibido = 0;
        ultimoAckMRecibido = -1;
        nroSecuenciaMEnviado = 0;
        ultimoAckMEnviado = 1;
    }

    string IpNumero;
    int Puerto;
    string NombreUsuario;
    Usuario *siguiente;
    Mensajes* mensa; // Los mensajes enviados para el usuario

    int nroSecuenciaRecibido; // En el que estoy del que recibí
    int ultimoAckEnviado; // Este es el que envié

    int ultimoAckRecibido; // Este es el que recibo
    int nroSecuenciaEnviado; // En el que estoy del que envié

    int ultimoAckMRecibido; // Este es el que recibo Multicast
    int nroSecuenciaMEnviado; // En el que estoy del que envié Multicast

    int nroSecuenciaMRecibido; // En el que estoy del que recibí Multicast
    int ultimoAckMEnviado; // Este es el que envié Multicast

    friend class Usuarios;

};

typedef Usuario *usuario;
typedef Mensajes* msgs;

class Usuarios {
public:
    int tope;
    usuario primero;
    usuario actual;

    Usuarios() {primero = actual = NULL; tope = 0;}
    ~Usuarios();

    bool Insertar(string ip, int port, string usr);
    void Borrar(string ip, string usr);

    bool ListaVacia() {return primero == NULL;}
    string Mostrar(string usr);
    void Siguiente();
    void Primero();
    void Ultimo();

    bool Actual() {return actual != NULL;}
    Usuario* Buscar(string usr); // Retorna la IP del usuario. Si devuelve vacìo el usario no existe, mas claro echale agua papi!

    Usuario* UsuarioActual() {return actual;}
    void AgregarMensaje(string msg, string usrReceptor);
    void BorrarMensaje(string usrEmisor);
    Mensaje* ObtenerMensaje(string usrEmisor);
    int ObtenerPuerto(string usr);
    string ObtenerIP(string usr);
    Usuario* BuscarIP(string usrIP);
    string ObtenerNombreIP(string ip);

    int ObtenerNroSecuenciaRecibido(string usr);
    int ObtenerUltimoACKRecibido(string usr);
    int ObtenerNroSecuenciaEnviado(string usr);
    int ObtenerUltimoACKEnviado(string usr);

    int ObtenerNroSecuenciaMRecibido(string usr);
    int ObtenerUltimoAckMRecibido(string usr);
    int ObtenerNroSecuenciaMEnviado(string usr);
    int ObtenerUltimoAckMEnviado(string usr);

    void InsertarAckRecibido(string usr, int ackRcv);
    void setUltimoAckEnviado(string usr);
    void setNroSecuenciaEnviado(string usr);
    void setAckRecibido(string usr);
    void setNroSecuenciaRecibido(string usr);

    void setUltimoAckMRecibido(string usr);
    void setNroSecuenciaMRecibido(string usr);
    void setUltimoAckMEnviado(string usr);
    void setNroSecuenciaMEnviado(string usr);
    void setUltimoACKMRecibido(string usr, int ACKM);
    bool llegoMensajeMulticast(int nroSecuencia);
    bool eliminarUsuario(string usr);
private:
    void convert(string& s);

};





#endif	/* USUARIOS_H */

