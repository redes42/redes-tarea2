#include "ServerSocket.h"
#include "SocketException.h"
#include "ServerSocketMulticast.h"
#include "Usuarios.h"
#include "Mensajes.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>


using namespace std;

const char* HOST_MULTICAST = "225.5.4.42";
const int PORT_MULTICAST = 54320;

string HOST_SERVER = "";
const int PORT_SERVER = 54321;
const int PORT_CLIENTE = 54321;
const int Tiempo_Espera = 1;

const int LOGIN = 0;
const int LOGOUT = 1;
const int GET_CONNECTED = 2;
const int MESSAGE = 3;
const int PRIVATE_MESSAGE = 4;
const int COMMAND_ERROR = 5;
const int TIEMPO_INACTIVIDAD = 360;


int CANT_CLIENTES_CONECTADOS;
int CANT_MENSAJES_ENVIADOS;
int CANT_CONEXIONES_TOTALES;

Usuarios * listaUsuarios;
Usuarios * listaMulticast;
ServerSocket * userSocket;
ServerSocketMulticast * multicastSocket;
clock_t tiempoInicial;

struct thread_data {
    char* name;
    char* ip;
    int port;
};

struct test {
    int var1, var2;
    float var3;
};

typedef struct test struct1;

typedef struct thread_data structThread;


pthread_mutex_t mutexSocketUnicast;
pthread_mutex_t mutexSocketMulticast;
pthread_mutex_t mutexMsjEnviadosUnicast;
pthread_mutex_t mutexMsjEnviadosMulticast;
pthread_mutex_t mutexNroSecuenciaMulticast;
pthread_mutex_t mutexMsjsUsuarios;


void* procesarHiloUsuario(void* datosHilo) {
    structThread *datosUsuario;
    datosUsuario = (structThread*) datosHilo;
    string nrosec;
    string usuario = datosUsuario->name;
    int NroSecEnviado;
    bool mate = false;
    int seg;
    time_t tiempoUltimaActividad; // Se inicia el tiempo de ejecucion
    time_t tiempoAhora; // Se inicia el tiempo de ejecucion
    time(&tiempoUltimaActividad);
    time(&tiempoAhora);
        
    
    double diferencia;
    try {
        while (!mate) {
            time(&tiempoAhora); // Se inicia el tiempo de ejecucion
            
            diferencia = difftime(tiempoAhora, tiempoUltimaActividad);

            if (diferencia > TIEMPO_INACTIVIDAD){
                pthread_mutex_lock(&mutexSocketUnicast);
                userSocket->write("GOODBYE 0", string(datosUsuario->ip), datosUsuario->port);
                string seg_string = std::to_string(seg);
               // cout << seg_string+"GOODBYE --lo manda a --"+ usuario+"\n";
                pthread_mutex_unlock(&mutexSocketUnicast);
                
                pthread_mutex_lock(&mutexMsjsUsuarios);
                //Envío el mensaje al usuario receptor que se indica   
                CANT_CLIENTES_CONECTADOS = CANT_CLIENTES_CONECTADOS - 1;
                bool eliminar = listaUsuarios->eliminarUsuario(usuario); 
                mate = true;
                pthread_mutex_unlock(&mutexMsjsUsuarios);
                
            }
            else{
                Mensaje * msg = listaUsuarios->ObtenerMensaje(usuario);
                if (msg != NULL) {
                    string data = msg->Texto;
                    int pos = data.find(" ", 0);
                    if (pos != string::npos) {
                        string commandName = data.substr(0, pos);
                        //Proceso mensaje privado
                        if (commandName == "PRIVATE_MESSAGE") {
                            //Envío el mensaje privado que me llegó                        
                            NroSecEnviado = listaUsuarios->ObtenerNroSecuenciaEnviado(usuario);
                            nrosec = std::to_string(NroSecEnviado);

                            while (NroSecEnviado != listaUsuarios->ObtenerUltimoACKRecibido(usuario)) {
                                // Espera para poder enviar el socket

                                pthread_mutex_lock(&mutexSocketUnicast);
                                userSocket->write(data + " " + nrosec, string(datosUsuario->ip), datosUsuario->port);
                               // cout << data + " " + nrosec +" --lo manda a --"+ usuario+"\n";
                                CANT_MENSAJES_ENVIADOS = CANT_MENSAJES_ENVIADOS + 1;
                                pthread_mutex_unlock(&mutexSocketUnicast);
                                // envio y seteo el fucking socket
                                // libera y deja que otro usuario pueda enviar un mensaje
                                sleep(Tiempo_Espera);
                            }

                            listaUsuarios->setNroSecuenciaEnviado(usuario); // Pongo el siguiente nro de secuencia que va a enviar.
                            //listaUsuarios->setAckRecibido(name);
                            //Elimino el mensaje de la cola de mensajes del usuario
                            //pthread_mutex_lock(&mutexMsjsUsuarios);
                            listaUsuarios->BorrarMensaje(usuario);
                            //pthread_mutex_unlock(&mutexMsjsUsuarios);

                        }
                        if (commandName == "GET_CONNECTED") {
                            NroSecEnviado = listaUsuarios->ObtenerNroSecuenciaEnviado(usuario);
                            nrosec = std::to_string(NroSecEnviado);
                            while (NroSecEnviado != listaUsuarios->ObtenerUltimoACKRecibido(usuario)) {

                                //Envío el mensaje al usuario receptor que se indica
                                string listaNombres = listaUsuarios->Mostrar(usuario);
                                pthread_mutex_lock(&mutexSocketUnicast);
                                userSocket->write("CONNECTED " + listaNombres + " " + nrosec, datosUsuario->ip, datosUsuario->port);

                                CANT_MENSAJES_ENVIADOS = CANT_MENSAJES_ENVIADOS + 1;
                                pthread_mutex_unlock(&mutexSocketUnicast);
                                // envio y seteo el fucking socket
                                // libera y deja que otro usuario pueda enviar un mensaje
                                sleep(Tiempo_Espera);
                                time(&tiempoUltimaActividad);
                            }
                            listaUsuarios->setNroSecuenciaEnviado(usuario); // Pongo el siguiente nro de secuencia que va a enviar.
                            //listaUsuarios->setAckRecibido(name);
                            //Elimino el mensaje de la cola de mensajes del usuario
                            //pthread_mutex_lock(&mutexMsjsUsuarios);
                            listaUsuarios->BorrarMensaje(usuario);
                            //pthread_mutex_unlock(&mutexMsjsUsuarios);


                        }
                        if (commandName == "ACK") {
                            //pthread_mutex_lock(&mutexMsjsUsuarios);

                            pthread_mutex_lock(&mutexSocketUnicast);
                            userSocket->write(data, string(datosUsuario->ip), datosUsuario->port);
                            pthread_mutex_unlock(&mutexSocketUnicast);
                            pthread_mutex_lock(&mutexMsjsUsuarios);
                            listaUsuarios->BorrarMensaje(usuario);
                            pthread_mutex_unlock(&mutexMsjsUsuarios);
                            //pthread_mutex_unlock(&mutexMsjsUsuarios);
                            time(&tiempoUltimaActividad);
                        }
                        if (commandName == "ACKM") {
                            //pthread_mutex_lock(&mutexMsjsUsuarios);

                            pthread_mutex_lock(&mutexSocketUnicast);
                            userSocket->write(data, string(datosUsuario->ip), datosUsuario->port);
                            pthread_mutex_unlock(&mutexSocketUnicast);
                            pthread_mutex_lock(&mutexMsjsUsuarios);
                            listaUsuarios->BorrarMensaje(usuario);
                            pthread_mutex_unlock(&mutexMsjsUsuarios);
                            //pthread_mutex_unlock(&mutexMsjsUsuarios);
                            time(&tiempoUltimaActividad);
                        }
                    }
                    if (data == "LOGOUT") {
                        pthread_mutex_lock(&mutexMsjsUsuarios);
                        //Envío el mensaje al usuario receptor que se indica   
                        CANT_CLIENTES_CONECTADOS = CANT_CLIENTES_CONECTADOS - 1;
                        listaUsuarios->BorrarMensaje(usuario);
                        bool eliminar = listaUsuarios->eliminarUsuario(usuario); 
                        mate = true;

                        pthread_mutex_unlock(&mutexMsjsUsuarios);
                    }
                }
            }
        }

        sleep(5000);
      //  pthread_mutex_lock(&mutexMsjsUsuarios);
        pthread_exit(0);
       // pthread_mutex_unlock(&mutexMsjsUsuarios);
    } catch (SocketException& e) {

    }
}

void* procesarHiloMulticast(void *) {
    try {
        string nrosec;
        while (true) {
            Mensaje * msg = listaMulticast->ObtenerMensaje("MULTICAST");
            if (msg != NULL) {
                string data = msg->Texto;

                bool noLlegoMensaje = true;
                int NroSecuencia;
                do  {
                    NroSecuencia = listaMulticast->ObtenerNroSecuenciaEnviado("MULTICAST");                                        
                    nrosec = to_string(NroSecuencia);
                    
                    multicastSocket->write(data + " " + nrosec);

                    pthread_mutex_lock(&mutexMsjEnviadosMulticast);
                    CANT_MENSAJES_ENVIADOS = CANT_MENSAJES_ENVIADOS + 1;
                    pthread_mutex_unlock(&mutexMsjEnviadosMulticast);

                    // envio y seteo el fucking socket
                    // libera y deja que otro usuario pueda enviar un mensaje
                    sleep(Tiempo_Espera);              
                    noLlegoMensaje = listaUsuarios->llegoMensajeMulticast(NroSecuencia);
                }
                while (noLlegoMensaje);

                listaMulticast->setNroSecuenciaEnviado("MULTICAST"); // Pongo el siguiente nro de secuencia que va a enviar.

                //Elimino el mensaje de la cola de mensajes de multicast
                listaMulticast->BorrarMensaje("MULTICAST");
            }
        }
    } catch (SocketException& e) {
        cout << "Se encontró una excepción:" << e.description() << "\nExiting.\n";
    }
}

int procesarDatos(string data, string ip, int port) {
    string nrosec;
    int NroSecuenciaRec;
    string NroSecuenciaRec_string;
    string ultimoACK_string;
    string name;

    int pos = data.find(" ", 0);
    if (pos != string::npos) {
        string commandName = data.substr(0, pos);
        if (commandName == "LOGIN") {

            //Obtengo el nombre del usuario a loggear
            string name_ACK = data.substr(6, data.length());
            
            int pos1 = name_ACK.find(" ", 0);
            name = name_ACK.substr(0, name_ACK.length() - 2);
            cout << ip+" LOGIN "+name+".\n";
            
            nrosec = data.substr(data.length() - 1, data.length());

            NroSecuenciaRec = listaUsuarios->ObtenerNroSecuenciaRecibido(name);
            if (NroSecuenciaRec == -1)
                NroSecuenciaRec = 0;
            NroSecuenciaRec_string = std::to_string(NroSecuenciaRec);
            if (nrosec != NroSecuenciaRec_string) {
                // Tengo que volver a enviar el ultimoAckEnviado
                int ultimoACK_enviado = listaUsuarios->ObtenerUltimoACKEnviado(name);
                ultimoACK_string = std::to_string(ultimoACK_enviado);
                listaUsuarios->AgregarMensaje("ACK " + ultimoACK_string, name);
            } else {
                pthread_t hiloUsuario;
                structThread *datosHilo;
                datosHilo = (structThread *) malloc(sizeof (structThread));
                datosHilo->ip = new char[ip.length() + 1];
                strcpy(datosHilo->ip, ip.c_str());
                datosHilo->name = new char[name.length() + 1];
                strcpy(datosHilo->name, name.c_str());
                datosHilo->port = port;
                
                string nombreIp = listaUsuarios->ObtenerIP(name);
                pthread_mutex_lock(&mutexMsjsUsuarios);
                //Agrego al usuario a la lista de usuarios           
                bool insertado = listaUsuarios->Insertar(ip, port, name);
                pthread_mutex_unlock(&mutexMsjsUsuarios);
                //TODO: manejo de errores ingreso de usuario repetido
                if (nombreIp != "") {
                    //cout<<"Existe el usuario, hay que mandar mensaje diciendo que ya existe al usr";
                    return -1;
                }else{
                    pthread_create(&hiloUsuario, NULL, procesarHiloUsuario, (void*) datosHilo);
                    CANT_CLIENTES_CONECTADOS = CANT_CLIENTES_CONECTADOS + 1;
                    CANT_CONEXIONES_TOTALES = CANT_CONEXIONES_TOTALES + 1;
                    listaUsuarios->AgregarMensaje("ACK " + nrosec, name);
                    listaUsuarios->setNroSecuenciaRecibido(name);
                    listaUsuarios->setUltimoAckEnviado(name);
                }
                
            }

            return 0;
        }
        if (commandName == "GET_CONNECTED") {
            cout << ip+" "+commandName+".\n";
            //cout << "------------------------------------------------\n";
            name = listaUsuarios->ObtenerNombreIP(ip);
            nrosec = data.substr(data.length() - 1, data.length());
            listaUsuarios->AgregarMensaje("ACK " + nrosec, name);
            listaUsuarios->setNroSecuenciaRecibido(name);
            listaUsuarios->setUltimoAckEnviado(name);
            listaUsuarios->AgregarMensaje(data, name);
            return 0;
        }
        if (commandName == "LOGOUT") {
            // elimino mensajes, usuario y thread
            string name = listaUsuarios->ObtenerNombreIP(ip);  
            if (name != ""){
                nrosec = data.substr(data.length()-1,data.length());
                NroSecuenciaRec = listaUsuarios->ObtenerNroSecuenciaRecibido(name);
                NroSecuenciaRec_string = std::to_string(NroSecuenciaRec);
                if (nrosec != NroSecuenciaRec_string){
                    // Tengo que volver a enviar el ultimoAckEnviado pero quizas ya elimine el usuario
                    int ultimoACK_enviado = listaUsuarios->ObtenerUltimoACKEnviado(name);
                    ultimoACK_string = std::to_string(ultimoACK_enviado);

                    listaUsuarios->AgregarMensaje("ACK " + ultimoACK_string, name); 
                } 
                else{

                    listaUsuarios->AgregarMensaje("ACK " + nrosec, name); 
                    listaUsuarios->setNroSecuenciaRecibido(name);
                    listaUsuarios->setUltimoAckEnviado(name); 
                    listaUsuarios->AgregarMensaje(commandName, name); 
                }                           
                       
                cout<<ip+" "+commandName+".\n";
                //cout<<ip;
            } 
            else{
                nrosec = data.substr(data.length()-1,data.length());

                pthread_mutex_lock(&mutexSocketUnicast);
                userSocket->write("ACK "+nrosec, ip, PORT_CLIENTE); 
                pthread_mutex_unlock(&mutexSocketUnicast);
                cout<<ip+" "+commandName+".\n";
            }
                
            return 0;
        }
        if (commandName == "ACK") { // Ultimo ACK Recibido;
            name = listaUsuarios->ObtenerNombreIP(ip);
            if (name != ""){
                listaUsuarios->setAckRecibido(name);

            }
            return 0;
        }
        if (commandName == "ACKM") { // Ultimo ACKM Recibido;
            string ACKM_string = data.substr(6, data.length());
            int ACKM = atoi( ACKM_string.c_str() );
            name = listaUsuarios->ObtenerNombreIP(ip);
            if (name != ""){
                if (listaUsuarios->ObtenerUltimoAckMRecibido(name) == -1){
                    listaUsuarios->setUltimoACKMRecibido(name, ACKM);
                }
                else
                    listaUsuarios->setUltimoAckMRecibido(name);
            }
            return 0;
        }
        if (commandName == "MESSAGE") {
            
            data = data.erase(data.find_last_not_of(" \n\r\t")+1);
            //Obtengo el text a enviar por multicast                
            string text = data.substr(pos + 1, data.length() - commandName.length() -3);
            name = listaUsuarios->ObtenerNombreIP(ip);
            cout<<ip+" "+commandName+" "+text+".\n";
            if (name != ""){
                nrosec = data.substr(data.length()-1, 1);

                int NroSecuenciaRec = listaUsuarios->ObtenerNroSecuenciaMRecibido(name);
                string NroSecuenciaRec_string = to_string(NroSecuenciaRec);

                if (nrosec != NroSecuenciaRec_string) {
                    // Tengo que volver a enviar el ultimoAckMulticast
                    int ultimoACKM_enviado = listaUsuarios->ObtenerUltimoAckMEnviado(name);
                    string ultimoACKM_string = to_string(ultimoACKM_enviado);
                    listaUsuarios->AgregarMensaje("ACKM " + ultimoACKM_string, name);
                } else {
                    listaUsuarios->AgregarMensaje("ACKM " + nrosec, name);                
                    listaMulticast->AgregarMensaje("RELAYED_MESSAGE " + name + " " + text, "MULTICAST");
                    listaUsuarios->setNroSecuenciaMRecibido(name);
                    listaUsuarios->setUltimoAckMEnviado(name);
                }
            }
            return 0;
            
        }
        if (commandName == "PRIVATE_MESSAGE") {
            int pos1 = data.find(" ", pos + 1);

            if (pos1 != string::npos) {
                string nameMsg = data.substr(pos + 1, pos1 - pos - 1);
                string text = data.substr(pos1 + 1, data.length() - 2 - (pos1 + 1));
                name = listaUsuarios->ObtenerNombreIP(ip);
                cout<<ip+" "+commandName+" "+nameMsg+" "+text+".\n";
                if (name != ""){
                    nrosec = data.substr(data.length() - 1, data.length());


                    NroSecuenciaRec = listaUsuarios->ObtenerNroSecuenciaRecibido(name);
                    NroSecuenciaRec_string = to_string(NroSecuenciaRec);
                    if (nrosec != NroSecuenciaRec_string) {
                        // Tengo que volver a enviar el ultimoAckEnviado
                        int ultimoACK_enviado = listaUsuarios->ObtenerUltimoACKEnviado(name);
                        ultimoACK_string = to_string(ultimoACK_enviado);
                        listaUsuarios->AgregarMensaje("ACK " + ultimoACK_string, name);
                    } else {
                        listaUsuarios->AgregarMensaje("ACK " + nrosec, name);
                        listaUsuarios->AgregarMensaje("PRIVATE_MESSAGE " + name + " " + text, nameMsg);
                        listaUsuarios->setNroSecuenciaRecibido(name);
                        listaUsuarios->setUltimoAckEnviado(name);
                    }
                }
                return 0;
            } else {
                return -1;
            }
        }
    }

    return -1;

}

void* leerConsola(void *) {
    //cout << "COMIENZA hilo leerConsola\n";
    string comandoLeido;
    time_t tiempoActual;
    time_t tiempoInicial;
    time(&tiempoInicial);
    string segString;
    while (true) {
        comandoLeido = "";
        cin >> comandoLeido;

        if (comandoLeido == "a") {
            //Mostrar cantidad de clientes conectados
            cout << "CANTIDAD DE CLIENTES CONECTADOS = ";
            cout << CANT_CLIENTES_CONECTADOS;
            cout << "\n";
        }

        if (comandoLeido == "s") {
            //Mostrar cantidad de mensajes enviados
            cout << "CANTIDAD DE MENSAJES ENVIADOS = ";
            cout << CANT_MENSAJES_ENVIADOS;
            cout << "\n";
        }

        if (comandoLeido == "d") {
            //Mostrar cantidad de conexiones totales
            cout << "CANTIDAD DE CONEXIONES TOTALES = ";
            cout << CANT_CONEXIONES_TOTALES;
            cout << "\n";
        }
        if (comandoLeido == "f") {
            time(&tiempoActual);
            double segundos = difftime(tiempoActual, tiempoInicial);
            int miliseg = segundos * 1000 / CLOCKS_PER_SEC;
            segString = to_string(segundos);
            cout<<"EL TIEMPO DE EJECUCIÓN ES "+segString+" SEGUNDOS.\n";
        }
    }
}

void* procesarMensajes(void *) {
    ServerSocket* server = new ServerSocket(HOST_SERVER, PORT_SERVER);
    string data;
    string ip;
    int port;
    try {
        while (true) {
            server->read(data, ip, port);
            procesarDatos(data, ip, PORT_CLIENTE);
        }
    }    
    catch (SocketException& e) {}
}

char* obtenerIPServidor() {
    FILE * fp = popen("ifconfig | grep '164.73' | awk -F' ' '{print $2}'", "r");
    char *p=NULL, *e; size_t n;
    if (fp) {
        if ((getline(&p, &n, fp) > 0) && p) {           
        }
    }
    pclose(fp);
    return p;
}

void inicializarVariables() {
    
    HOST_SERVER = string(obtenerIPServidor());
    
    //Inicializo las cantidades
    CANT_CLIENTES_CONECTADOS = 0;
    CANT_MENSAJES_ENVIADOS = 0;
    CANT_CONEXIONES_TOTALES = 0;

    //Inicializo los mutex
    pthread_mutex_init(&mutexSocketUnicast, NULL);
    pthread_mutex_init(&mutexSocketMulticast, NULL);
    pthread_mutex_init(&mutexMsjEnviadosUnicast, NULL);
    pthread_mutex_init(&mutexMsjEnviadosMulticast, NULL);
    pthread_mutex_init(&mutexNroSecuenciaMulticast, NULL);    
    pthread_mutex_init(&mutexMsjsUsuarios, NULL);    
    

    //Inicializo sockets
    userSocket = new ServerSocket(HOST_SERVER, 0);
    multicastSocket = new ServerSocketMulticast(HOST_MULTICAST, PORT_MULTICAST);

    //Inicializo las listas de Usuarios y la lista que contiene al "usuario" Multicast
    listaUsuarios = new Usuarios();
    listaMulticast = new Usuarios();

    pthread_t hiloMulticast;   

    //Agrego el único usuario a la lista de multicast
    listaMulticast->Insertar(HOST_MULTICAST, PORT_MULTICAST, "MULTICAST");
    
    //Creo hilo para atender MULTICAST
    pthread_create(&hiloMulticast, NULL, procesarHiloMulticast, NULL);
}



int main() {
    
    tiempoInicial = clock();
    inicializarVariables();

    cout << "------------------------------------------------\n";
    cout << "Ejecutando SERVER en la IP = ";
    cout << HOST_SERVER;
    cout << "------------------------------------------------\n";
    
    pthread_t hiloLeerConsola;
    pthread_t hiloProcesarMensajes;

    if (pthread_create(&hiloLeerConsola, NULL, leerConsola, NULL)) {
        printf("Error en la creación del hiloLeerConsola\n");
        exit(1);
    }
    if (pthread_create(&hiloProcesarMensajes, NULL, procesarMensajes, NULL)) {
        printf("Error en la creación del hiloProcesarMensajes\n");
        exit(1);
    }
    while (true) {
        //Sleep para esperar que termine la ejecución del Server
    }
    return 0;
}


